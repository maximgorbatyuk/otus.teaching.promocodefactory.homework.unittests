﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController : ControllerBase
    {
        private readonly IPartnerService _partnerService;
        public PartnersController(IPartnerService partnerService)
        {
            _partnerService = partnerService;
        }

        [HttpGet]
        public async Task<ActionResult<IReadOnlyCollection<PartnerResponse>>> GetPartnersAsync()
        {
            var response = await _partnerService.GetPartnersAsync();
            return Ok(response);
        }
        
        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimitResponse>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            try
            {
                var response = await _partnerService.GetPartnerLimitAsync(id, limitId);
                return Ok(response);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
        }
        
        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            try
            {
                var response = await _partnerService.SetPartnerPromoCodeLimitAsync(id, request);
                return CreatedAtAction(nameof(GetPartnerLimitAsync), response, null);
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (SetPartnerPromoCodeLimitException e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            try
            {
                await _partnerService.CancelPartnerPromoCodeLimitAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (SetPartnerPromoCodeLimitException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}