﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Applications.Services
{
    public class PartnerService: 
        IPartnerService
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;

        public PartnerService(IRepository<Partner> partnersRepository, ICurrentDateTimeProvider currentDateTimeProvider)
        {
            _partnersRepository = partnersRepository;
            _currentDateTimeProvider = currentDateTimeProvider;
        }
        
        public async Task<IReadOnlyCollection<PartnerResponse>> GetPartnersAsync()
        {
            return (await _partnersRepository.GetAllAsync())
                .Select(x => new PartnerResponse(x))
                .ToArray();
        }

        public async Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var partner = await _partnersRepository.ByIdOrFailAsync(id);

            return new PartnerPromoCodeLimitResponse(
                partner.Limit(limitId));
        }

        public async Task<SetPartnerPromoCodeLimitResponse> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            if (request.Limit <= 0)
                throw new SetPartnerPromoCodeLimitException("Лимит должен быть больше 0");

            Partner partner = await _partnersRepository.ByIdOrFailAsync(id);

            partner.AddLimit(
                createDate: _currentDateTimeProvider.CurrentDateTime,
                endDate: request.EndDate,
                limit: request.Limit);

            await _partnersRepository.UpdateAsync(partner);

            return new SetPartnerPromoCodeLimitResponse(partner);
        }

        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            Partner partner = await _partnersRepository.ByIdOrFailAsync(id);

            partner.Cancel(_currentDateTimeProvider.CurrentDateTime);
            
            // Отключение лимита
            partner.PartnerLimits
                .FirstOrDefault(x => !x.CancelDate.HasValue)
                ?.Cancel(_currentDateTimeProvider.CurrentDateTime);

            await _partnersRepository.UpdateAsync(partner);

        }
    }
}