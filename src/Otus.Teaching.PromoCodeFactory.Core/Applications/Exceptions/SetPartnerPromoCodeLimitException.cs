﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Applications.Exceptions
{
    public class SetPartnerPromoCodeLimitException:
        Exception
    {
        public SetPartnerPromoCodeLimitException()
        {
            
        }
        
        public SetPartnerPromoCodeLimitException(string message)
            : base(message)
        {
            
        }
        
        public SetPartnerPromoCodeLimitException(string message, Exception exception)
            : base(message, exception)
        {
            
        }

    }
}