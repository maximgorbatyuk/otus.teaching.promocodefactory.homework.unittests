﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Applications.Dto
{
    public class PartnerPromoCodeLimitResponse
    {
        public PartnerPromoCodeLimitResponse(PartnerPromoCodeLimit y)
        {
            Id = y.Id;
            PartnerId = y.PartnerId;
            Limit = y.Limit;
            CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss");
            EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss");
            CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss");
        }

        public Guid Id { get; }

        public Guid PartnerId { get; }

        public string CreateDate { get; }

        public string CancelDate { get; }

        public string EndDate { get; }

        public int Limit { get; }
    }
}