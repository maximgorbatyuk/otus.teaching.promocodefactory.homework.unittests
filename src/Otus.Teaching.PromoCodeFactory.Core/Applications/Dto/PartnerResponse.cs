﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Applications.Dto
{
    public class PartnerResponse
    {
        public PartnerResponse(Partner partner)
        {
            Id = partner.Id;
            Name = partner.Name;
            NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
            IsActive = partner.IsActive;
            PartnerLimits = partner.PartnerLimits
                .Select(y => new PartnerPromoCodeLimitResponse(y))
                .ToArray();
        }

        public Guid Id { get; }

        public bool IsActive { get; }
        
        public string Name { get; }

        public int NumberIssuedPromoCodes  { get; }

        public IReadOnlyCollection<PartnerPromoCodeLimitResponse> PartnerLimits { get; }
    }
}