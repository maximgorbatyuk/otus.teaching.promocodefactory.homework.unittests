﻿using System;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Applications.Dto
{
    public class SetPartnerPromoCodeLimitResponse
    {
        public SetPartnerPromoCodeLimitResponse(Partner partner)
        {
            PartnerId = partner.Id;
            LimitId = partner.PartnerLimits.Last().Id;
        }


        public Guid PartnerId { get; }
        
        public Guid LimitId { get; }
    }
}