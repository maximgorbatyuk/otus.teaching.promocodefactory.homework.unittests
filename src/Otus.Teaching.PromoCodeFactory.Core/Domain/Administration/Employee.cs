﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Applications;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : BaseEntity
    {
        protected Employee()
        {
        }

        public Employee(string firstName, string lastName, string email, Role role, int appliedPromocodesCount)
        {
            FirstName = firstName.ThrowIfNullOrEmpty(nameof(firstName));
            LastName = lastName.ThrowIfNullOrEmpty(nameof(lastName));
            Email = email.ThrowIfNullOrEmpty(nameof(email));
            RoleId = role.ThrowIfNull(nameof(role)).Id;
            AppliedPromocodesCount = appliedPromocodesCount;
        }

        public string FirstName { get; protected set; }

        public string LastName { get; protected set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; protected set; }

        public Guid RoleId { get; protected set; }

        public virtual Role Role { get; protected set; }

        public int AppliedPromocodesCount { get; protected set; }
    }
}