﻿using Otus.Teaching.PromoCodeFactory.Core.Applications;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role : BaseEntity
    {
        protected Role()
        {
        }

        public Role(string name, string description)
        {
            Name = name.ThrowIfNullOrEmpty(nameof(name));
            Description = description.ThrowIfNullOrEmpty(nameof(description));
        }

        public string Name { get; protected set; }

        public string Description { get; protected set; }
    }
}