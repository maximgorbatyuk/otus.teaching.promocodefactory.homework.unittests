﻿using Otus.Teaching.PromoCodeFactory.Core.Applications;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference : BaseEntity
    {
        protected Preference()
        {
        }

        public Preference(string name)
        {
            Name = name.ThrowIfNullOrEmpty(nameof(name));
        }

        public string Name { get; protected set; }
    }
}