﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Applications;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
    {
        protected CustomerPreference()
        {
        }

        public CustomerPreference(Customer customer, Preference preference)
        {
            CustomerId = customer.ThrowIfNull(nameof(customer)).Id;
            PreferenceId = preference.ThrowIfNull(nameof(preference)).Id;
        }

        public CustomerPreference(Preference preference)
        {
            PreferenceId = preference.ThrowIfNull(nameof(preference)).Id;
        }

        public Guid CustomerId { get; protected set; }

        public virtual Customer Customer { get; protected set; }

        public Guid PreferenceId { get; protected set; }

        public virtual Preference Preference { get; protected set; }
    }
}