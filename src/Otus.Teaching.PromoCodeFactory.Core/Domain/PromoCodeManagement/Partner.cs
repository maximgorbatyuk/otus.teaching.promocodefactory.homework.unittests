﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Applications;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Partner : BaseEntity
    {
        protected Partner()
        {
        }

        public Partner(string name, bool isActive, int numberIssuedPromoCodes, params PartnerPromoCodeLimit[] limits)
        {
            Name = name.ThrowIfNullOrEmpty(nameof(name));
            IsActive = isActive;
            NumberIssuedPromoCodes = numberIssuedPromoCodes;
            PartnerLimits = new List<PartnerPromoCodeLimit>(limits);
        }

        public Partner(string name, bool isActive, params PartnerPromoCodeLimit[] limits)
        {
            Name = name.ThrowIfNullOrEmpty(nameof(name));
            IsActive = isActive;
            PartnerLimits = new List<PartnerPromoCodeLimit>(limits);
        }

        public string Name { get; protected set; }

        public int NumberIssuedPromoCodes  { get; protected set; }

        public bool IsActive { get; protected set; }

        public virtual ICollection<PartnerPromoCodeLimit> PartnerLimits { get; protected set; }

        protected IReadOnlyCollection<PartnerPromoCodeLimit> LimitsWithoutCancelDate =>
            PartnerLimits
                .Where(x => !x.CancelDate.HasValue)
                .ToArray();

        public void AddLimit(DateTime createDate, DateTime endDate, int limit)
        {
            // Если партнер заблокирован, то нужно выдать исключение
            if (!IsActive)
                throw new SetPartnerPromoCodeLimitException("Данный партнер не активен");

            // Установка лимита партнеру
            foreach (PartnerPromoCodeLimit codeLimit in LimitsWithoutCancelDate)
            {
                // Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал,
                // если лимит закончился, то количество не обнуляется
                NumberIssuedPromoCodes = 0;

                // При установке лимита нужно отключить предыдущий лимит
                codeLimit.Cancel(createDate);
            }

            PartnerLimits.Add(new PartnerPromoCodeLimit(
                partner: this,
                createDate: createDate,
                cancelDate: null,
                endDate: endDate,
                limit: limit));
        }

        public void Cancel(DateTime cancelDate)
        {
            // Если партнер заблокирован, то нужно выдать исключение
            if (!IsActive)
                throw new SetPartnerPromoCodeLimitException("Данный партнер не активен");

            // Отключение лимита
            foreach (PartnerPromoCodeLimit limit in PartnerLimits.Where(x => !x.CancelDate.HasValue))
            {
                limit.Cancel(cancelDate);
            }
        }

        public Partner Disable()
        {
            if (!IsActive)
            {
                throw new InvalidOperationException();
            }

            IsActive = false;
            return this;
        }

        public PartnerPromoCodeLimit Limit(Guid limitId)
        {
            return PartnerLimits.FirstOrDefault(x => x.Id == limitId)
                   ?? throw new EntityNotFoundException(limitId);
        }
    }
}