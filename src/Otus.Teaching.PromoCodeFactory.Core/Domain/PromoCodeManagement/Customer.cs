﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Applications;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {
        protected Customer()
        {
        }

        public Customer(string firstName, string lastName, string email, params CustomerPreference[] preferences)
        {
            FirstName = firstName.ThrowIfNullOrEmpty(nameof(firstName));
            LastName = lastName.ThrowIfNullOrEmpty(nameof(lastName));
            Email = email.ThrowIfNullOrEmpty(nameof(email));

            Preferences = new List<CustomerPreference>(preferences);
        }

        public string FirstName { get; protected set; }

        public string LastName { get; protected set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; protected set; }

        public virtual ICollection<CustomerPreference> Preferences { get; protected set; }

        public Customer Add(IEnumerable<Preference> preferences)
        {
            Preferences.Clear();

            foreach (Preference preference in preferences)
            {
                Preferences.Add(new CustomerPreference(this, preference));
            }

            return this;
        }

        public void Edit(string firstName, string lastName, string email)
        {
            FirstName = firstName.ThrowIfNullOrEmpty(nameof(firstName));
            LastName = lastName.ThrowIfNullOrEmpty(nameof(lastName));
            Email = email.ThrowIfNullOrEmpty(nameof(email));
        }
    }
}