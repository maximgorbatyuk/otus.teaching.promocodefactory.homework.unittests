﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Applications;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PartnerPromoCodeLimit : BaseEntity
    {
        protected PartnerPromoCodeLimit()
        {
        }

        public PartnerPromoCodeLimit(DateTime createDate, DateTime? cancelDate, DateTime endDate, int limit)
        {
            CreateDate = createDate;
            CancelDate = cancelDate;
            EndDate = endDate;
            Limit = limit;
        }

        public PartnerPromoCodeLimit(DateTime createDate, DateTime endDate, int limit)
            : this(createDate, null, endDate, limit)
        {
        }

        public PartnerPromoCodeLimit(
            Partner partner, DateTime createDate, DateTime? cancelDate, DateTime endDate, int limit)
            : this(createDate, cancelDate, endDate, limit)
        {
            PartnerId = partner.ThrowIfNull(nameof(partner)).Id;
        }

        public Guid PartnerId { get; protected set; }

        public virtual Partner Partner { get; protected set; }
        
        public DateTime CreateDate { get; protected set; }

        public DateTime? CancelDate { get; protected set; }

        public DateTime EndDate { get; protected set; }

        public int Limit { get; protected set; }

        public void Cancel(DateTime cancelDate)
        {
            if (CancelDate.HasValue)
            {
                throw new InvalidOperationException();
            }

            CancelDate = cancelDate;
        }
    }
}