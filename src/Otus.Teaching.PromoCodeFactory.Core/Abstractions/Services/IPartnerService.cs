﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Dto;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IPartnerService
    {
        Task<IReadOnlyCollection<PartnerResponse>> GetPartnersAsync();

        Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId);

        Task<SetPartnerPromoCodeLimitResponse> SetPartnerPromoCodeLimitAsync(Guid id,
            SetPartnerPromoCodeLimitRequest request);

        Task CancelPartnerPromoCodeLimitAsync(Guid id);
    }
}