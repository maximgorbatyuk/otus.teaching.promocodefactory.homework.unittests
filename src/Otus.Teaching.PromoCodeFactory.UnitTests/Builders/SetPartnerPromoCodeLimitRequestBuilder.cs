﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Dto;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class SetPartnerPromoCodeLimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateBase()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 10,
                EndDate = new DateTime(2021, 02, 01)
            };
        }

        public static SetPartnerPromoCodeLimitRequest WithLimit(this SetPartnerPromoCodeLimitRequest request, int limit)
        {
            request.Limit = limit;
            return request;
        }
    }
}