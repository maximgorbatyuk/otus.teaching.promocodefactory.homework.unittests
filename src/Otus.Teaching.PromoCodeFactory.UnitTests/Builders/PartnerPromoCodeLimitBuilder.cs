﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerPromoCodeLimitBuilder
    {
        public static PartnerPromoCodeLimit CreateBase()
        {
            return new PartnerPromoCodeLimit(
                createDate: new DateTime(2021, 01, 01),
                endDate: new DateTime(2021, 02, 01),
                limit: 20);
        }

        public static PartnerPromoCodeLimit WithCancelDate(this PartnerPromoCodeLimit limit, DateTime cancelDate)
        {
            limit.Cancel(cancelDate);
            return limit;
        }
    }
}