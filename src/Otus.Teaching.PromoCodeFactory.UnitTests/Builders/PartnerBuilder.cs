﻿using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public  static class PartnerBuilder
    {
        public static Partner CreateBase()
        {
            return new Partner(
                name: "PartnerName",
                isActive: true,
                numberIssuedPromoCodes: 5);
        }

        public static Partner WithLimit(this Partner partner, PartnerPromoCodeLimit limit)
        {
            partner.PartnerLimits.Add(new PartnerPromoCodeLimit(
                partner: partner,
                createDate: limit.CreateDate,
                cancelDate: limit.CancelDate,
                endDate: limit.EndDate,
                limit: limit.Limit));

            return partner;
        }

        public static Partner WithIssuedPromoCodes(this Partner partner, int number)
        {
            return new Partner(
                name: partner.Name,
                isActive: partner.IsActive,
                numberIssuedPromoCodes: number,
                partner.PartnerLimits.ToArray());
        }
    }
}