﻿using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Exceptions;
using Otus.Teaching.PromoCodeFactory.UnitTests.Attributes;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound(
            [Frozen] Mock<IPartnerService> partnerService,
            [Frozen, NoAutoProperties] PartnersController partnersController)
        {
            //arrange
            var partnerId = PartnerBuilder.CreateBase().Id;
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            
            partnerService.Setup(service =>
                    service.SetPartnerPromoCodeLimitAsync(partnerId,request ))
                .Throws<EntityNotFoundException>();
            
            //act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId,
                request);
            
            //assert
            result.Should().BeAssignableTo<NotFoundObjectResult>();
        }

        [Theory, AutoMoqData]
        public async Task SetPartnersPromoCodeLimitAsync_PartnerIsInactive_ThrowsSetPartnerPromoCodeLimitException(
            [Frozen] Mock<IPartnerService> partnerService,
            [Frozen, NoAutoProperties] PartnersController partnersController)
        {
            //arrange
            var partnerId = PartnerBuilder.CreateBase().Disable().Id;
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            partnerService.Setup(service =>
                    service.SetPartnerPromoCodeLimitAsync(partnerId, request))
                .Throws<SetPartnerPromoCodeLimitException>();
            
            //act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            //assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}