﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Attributes;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Applications.Services.PartnerServiceTest
{
    public class SetPartnersPromoCodeLimitAsyncTests
    {
        [Theory, AutoMoqData]
        //Важен порядок аргументов в тесте!!!
        public async Task SetPartnersPromoCodeLimitAsync_PartnerIsNotFound_ThrowsEntityNotFountException(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var partnerId = PartnerBuilder.CreateBase().Id;
            partnerRepository.Setup(repo => repo.ByIdOrFailAsync(partnerId))
                .ThrowsAsync(new EntityNotFoundException());
            
            //act
            Func<Task<SetPartnerPromoCodeLimitResponse>> act = async () =>  await partnerService.SetPartnerPromoCodeLimitAsync(
                partnerId, SetPartnerPromoCodeLimitRequestBuilder.CreateBase());
            
            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqData]
        public async Task SetPartnersPromoCodeLimitAsync_PartnerIsInactive_ThrowsSetPartnerPromoCodeLimitException(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var partner = PartnerBuilder.CreateBase().Disable();
            partnerRepository.Setup(repo => repo.ByIdOrFailAsync(partner.Id))
                .ReturnsAsync(partner);
            
            //act
            Func<Task<SetPartnerPromoCodeLimitResponse>> act = async () =>
                await partnerService.SetPartnerPromoCodeLimitAsync(
                    partner.Id, SetPartnerPromoCodeLimitRequestBuilder.CreateBase());
            
            //assert
            await act.Should().ThrowAsync<SetPartnerPromoCodeLimitException>();
        }

        [Theory, AutoMoqData]
        public async Task SetPartnersPromoCodeLimitAsync_PreviousLimitIsActive_NumberIssuedPromoCodesIsSetToZero(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var partner = PartnerBuilder.CreateBase()
                .WithLimit(
                    PartnerPromoCodeLimitBuilder.CreateBase());

            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            partnerRepository.Setup(repo => repo.ByIdOrFailAsync(partner.Id))
                .ReturnsAsync(partner);
            
            //act
            var result = await partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Theory, AutoMoqData]
        public async Task SetPartnersPromoCodeLimitAsync_PreviousLimitIsNotActive_NumberIssuedPromoCodesIsNotChanged(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var limit = PartnerPromoCodeLimitBuilder.CreateBase()
                .WithCancelDate(new DateTime(2021, 01, 15));

            var partner = PartnerBuilder.CreateBase()
                .WithLimit(limit)
                .WithIssuedPromoCodes(4);

            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            partnerRepository.Setup(repo => repo.ByIdOrFailAsync(partner.Id))
                .ReturnsAsync(partner);
            
            //act
            await partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //assert
            partner.NumberIssuedPromoCodes.Should().Be(4);
        }
        
        [Theory, AutoMoqData]
        public async Task SetPartnersPromoCodeLimitAsync_PreviousLimitIsActive_PreviousLimitIsNotActive(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] Mock<ICurrentDateTimeProvider> currentDateTimeProvider,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var partner = PartnerBuilder.CreateBase()
                .WithLimit(PartnerPromoCodeLimitBuilder.CreateBase());

            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();

            partnerRepository.Setup(repo => repo.ByIdOrFailAsync(partner.Id))
                .ReturnsAsync(partner);
            currentDateTimeProvider.Setup(provider => provider.CurrentDateTime)
                .Returns(new DateTime(2021, 01, 15));
            
            //act
            await partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //assert
            Assert.Equal(2, partner.PartnerLimits.Count);
            partner.PartnerLimits
                .First()
                .CancelDate
                .Should().Be(currentDateTimeProvider.Object.CurrentDateTime);
        }

        [Theory, AutoMoqData]
        public async Task SetPartnersPromoCodeLimitAsync_RequestLimitIsZero_ThrowsSetPartnerPromoCodeLimitException(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var partner = PartnerBuilder.CreateBase();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase().WithLimit(0);
            partnerRepository.Setup(repo => repo.ByIdOrFailAsync(partner.Id))
                .ReturnsAsync(partner);
            
            //act
            Func<Task<SetPartnerPromoCodeLimitResponse>> act = async () =>
                await partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //assert
            await act.Should().ThrowAsync<SetPartnerPromoCodeLimitException>("Лимит должен быть больше 0");
        }

        [Theory, AutoMoqData]
        public async Task SetPartnersPromoCodeLimitAsync_CorrectRequest_RepositoryUpdateCalled(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] Mock<ICurrentDateTimeProvider> currentDateTimeProvider,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var partner = PartnerBuilder.CreateBase();
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase();
            partnerRepository.Setup(repo => repo.ByIdOrFailAsync(partner.Id))
                .ReturnsAsync(partner);
            currentDateTimeProvider.Setup(provider => provider.CurrentDateTime)
                .Returns(new DateTime(2021, 01, 15));
            
            //act
            await partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            partnerRepository.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }
    }
}