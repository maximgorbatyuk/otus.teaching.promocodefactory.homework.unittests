﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Applications.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Attributes;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Applications.Services.PartnerServiceTest
{
    public class GetPartnersAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetPartnersAsync_PartnersExists_ReturnsPartnersWithoutLimits(
            [Frozen] Mock<IRepository<Partner>> partnerRepository,
            [Frozen] PartnerService partnerService)
        {
            //arrange
            var partner = PartnerBuilder.CreateBase();
            var partnersCollection = new List<Partner>()
            {
                partner
            };

            partnerRepository
                .Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(partnersCollection);
            
            //act
            var result = await partnerService.GetPartnersAsync();

            //assert
            result.Should().BeEquivalentTo(partnersCollection);
        }
    }
}