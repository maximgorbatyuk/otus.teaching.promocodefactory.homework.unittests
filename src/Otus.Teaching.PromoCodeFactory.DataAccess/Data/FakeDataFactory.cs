﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Role> Roles = new List<Role>()
        {
            new Role("Admin", "Администратор"),
            new Role("PartnerManager", "Партнерский менеджер")
        };

        public static List<Employee> Employees = new List<Employee>()
        {
            new Employee(
                firstName: "Иван", 
                lastName: "Сергеев",
                email: "owner@somemail.ru",
                role: Roles.First(x => x.Name == "Admin"),
                appliedPromocodesCount: 5),
            new Employee(
                firstName: "Петр",
                lastName: "Андреев",
                email: "andreev@somemail.ru",
                role: Roles.First(x => x.Name == "PartnerManager"),
                appliedPromocodesCount: 10),
        };

        public static List<Preference> Preferences = new List<Preference>()
        {
            new Preference("Театр"),
            new Preference("Семья"),
            new Preference("Дети")
        };

        public static List<Customer> Customers = new List<Customer>()
        {
            new Customer(
                firstName: "Иван",
                lastName: "Петров",
                email: "ivan_sergeev@mail.ru",
                new CustomerPreference(Preferences[0]),
                new CustomerPreference(Preferences[1]))
        };

        public static List<Partner> Partners = new List<Partner>()
        {
            new Partner(
                name: "Суперигрушки",
                isActive: true,
                new PartnerPromoCodeLimit(
                    createDate: new DateTime(2020,07,9),
                    endDate: new DateTime(2020,10,9),
                    limit: 100)),
            new Partner(
                name: "Каждому кота",
                isActive: true,
                new PartnerPromoCodeLimit(
                    createDate: new DateTime(2020,05,3),
                    cancelDate: new DateTime(2020,10,15),
                    endDate: new DateTime(2020,06,16),
                    limit: 1000),
                new PartnerPromoCodeLimit(
                    createDate: new DateTime(2020,05,3),
                    endDate: new DateTime(2020,10,15),
                    limit: 100)),
            new Partner(
                name: "Рыба твоей мечты",
                isActive: false,
                new PartnerPromoCodeLimit(
                    createDate: new DateTime(2020,07,3),
                    endDate: new DateTime(2020,9,9),
                    limit: 100))
        };
    }
}